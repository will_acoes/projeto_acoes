#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   time_progress.py
#
#   The MIT License (MIT)
#
#   Copyright (c) 2019 Willian <will1dexter@will1dexter-pc>.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy
#   of this software and associated documentation files (the "Software"), to deal
#   in the Software without restriction, including without limitation the rights
#   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#   copies of the Software, and to permit persons to whom the Software is
#   furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#   THE SOFTWARE.
#
#
 
"""
Módulo
 
Este módulo contém um componente de barra de progresso customizada.
"""
 
__author__ = "Willian Antônio dos Santos"
__copyright__ = "Copyright 2019"
__credits__ = ["Willian Antônio dos Santos"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Willian Antônio dos Santos"
__email__ = "Will1Dexter@hotmail.com"
__status__ = "Production"

# ---- Imports:
import time
import sys

# ---- Componente para apresentação de tempo e progresso:
class Time_Progress(object):
    def __init__(self, decimals=1, length=100, fill='█'):
        self.decimals = decimals
        self.length = length
        self.fill = fill
 
        self.last_time = time.time()
 
    def get_formatted_time(self, t_duration_time=None, update_time=False):
        if(t_duration_time is None):
            t_duration_time = (time.time() - self.last_time)
            if(update_time):
                self.last_time = time.time()
 
        vec_time = []
        vec_time = vec_time + [(int(t_duration_time * 1000000) % 1000)]
        vec_time = vec_time + [int(t_duration_time * 1000) % 1000]
        vec_time = vec_time + [int(t_duration_time) % 60]
        vec_time = vec_time + [int(t_duration_time / 60) % 60]
        vec_time = vec_time + [int(t_duration_time / 3600) % 24]
        vec_time = vec_time + [int(t_duration_time / 86400)]
        return vec_time
 
    def get_more_relevant_vec_time(self, vec_time):
        more_relevant = 0
        for i in range(len(vec_time)):
            j = (len(vec_time) - 1 - i)
            v_t = vec_time[j]
            if(v_t > 0):
                more_relevant = j
                break
 
        return more_relevant
 
    def get_unit_vec_time(self, index_vec_time):
        if(index_vec_time == 0):
            return u'mic'
        elif(index_vec_time == 1):
            return u'mli'
        elif(index_vec_time == 2):
            return u'sec'
        elif(index_vec_time == 3):
            return u'min'
        elif(index_vec_time == 4):
            return u'hs '
        else:
            return u'day'
 
    def get_formatted_time_str(self, vec_time, more_relevant=-1, num_fields_show=2):
        str_f_time = u''
        if(more_relevant >= 0):
            str_f_time += ("{:3d}").format(vec_time[more_relevant]) + u' ' + self.get_unit_vec_time(more_relevant)
        else:
            num_fields = 0
            for i in range(len(vec_time)):
                j = (len(vec_time) - 1 - i)
                if((num_fields < num_fields_show) and (vec_time[j] > 0)):
                    str_f_time += ("{:3d}").format(vec_time[j]) + u' ' + self.get_unit_vec_time(j)
                    if(num_fields < (num_fields_show - 1)):
                        str_f_time += u' '
                    num_fields += 1
 
        return str_f_time
 
    # Print iterations progress by Greenstick (https://stackoverflow.com/questions/3173320/text-progress-bar-in-the-console)
    def print_progress_bar(self, iteration, total, prefix='', suffix='', msg_before=u'', msg_finalize=u'', new_line=False):
        percent = ("{0:" + str(self.decimals + 4) + "." + str(self.decimals) + "f}").format(100 * (iteration / float(total)))
        filled_length = int(self.length * iteration // total)
        bar_fills = self.fill * filled_length + '-' * (self.length - filled_length)
 
        if(len(msg_before) > 0):
            if(new_line):
                padding_str = u' ' * ((6 + len(prefix) + len(bar_fills) + len(percent) + len(suffix)) - len(msg_before))
                sys.stdout.write('\r%s\n%s |%s| %s%% %s' % ((msg_before + padding_str), prefix, bar_fills, percent, suffix))
            else:
                sys.stdout.write('\r%s %s |%s| %s%% %s' % (msg_before, prefix, bar_fills, percent, suffix))
            sys.stdout.flush()
        else:
            sys.stdout.write('\r%s |%s| %s%% %s' % (prefix, bar_fills, percent, suffix))
            sys.stdout.flush()
 
        if iteration == total: 
            print(msg_finalize)
     
    def print_progress_bar_time(self, iteration, total, msg_before=u'', msg_finalize=u'', new_line=False):
        if(iteration > total):
            iteration = total
 
        t_total = (time.time() - self.last_time)
        str_time_est = u' ' + u''.join([u'*'] * 14)
        if(iteration > 0):
            t_estimate = (t_total / float(iteration)) * float(total - iteration) + t_total
 
            vec_time_estimate = self.get_formatted_time(t_estimate)
 
            str_time_est = self.get_formatted_time_str(vec_time_estimate)
 
        vec_time_elapsed = self.get_formatted_time(t_total)
 
        str_time = u'est:' + str_time_est + u' | elap:' + self.get_formatted_time_str(vec_time_elapsed)
 
        self.print_progress_bar(iteration, total, prefix=u'->', suffix=str_time, msg_before=msg_before, msg_finalize=msg_finalize, new_line=new_line)


