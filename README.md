# Will_Acões : Script de Análise Fundamentalista de Ações #

*Projeto de avaliação de ações de empresas da IBOVESPA por análise fundamentalista. Baseado no projeto [https://github.com/Doraflara/financas-r](https://github.com/Doraflara/financas-r)*

### Conteúdo deste repositório: ###

* Contém um script em python que realiza análise fundamentalista de ações de empresas da IBOVESPA.

### Dependência: ###

* **Todas as dependências estão no arquivo requirements.txt, são basicamente**:
	* python3 - python versão 3 com o pip (gerenciador de pacotes do python);
	* virtualenv - ferramenta para criar ambientes virtuais (isolando as dependências desse projeto do resto do sistema);
	* beautifulsoup4 - biblioteca para parsear o DOM dos sites onde são coletados os dados das empresas;
	* mechanize - biblioteca para coletar dados dos sites;
	* numpy - biblioteca de cálculo matemático de alto desempenho;
	* openpyxl - biblioteca para ler e escrever planilhas do excel;
	* pandas - biblioteca para manipular data frames (big data);
	* scikit-learn - biblioteca de machine learn (usada para fazer regressão linear).
	
### Como configurar o repositório no Windows: ###

1. Clonar o repositório:
    * Abra o console de comando do sistema operacional e navegue até o local onde será clonado o repositório;
    * Abra a pagina de [`Overview`](https://bitbucket.org/will_acoes/projeto_acoes/overview) deste repositório, clique em clonar, copie o link HTTPS e execute os comandos abaixo;
    * `git clone link_HTTPS_copiado Projeto_Acoes`;
    * Digite sua senha do Bitbucket quando exigido e `Enter`;
2. Instalar o python3:
	* Instalar o python versão 3 (Latest Python 3 Release - Python 3.X.X) para windows na url [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/).
3. Usando o pip (gerenciador de pacotes do python) instale o virtualenv (ambiente virtual):
	* Execute no prompt de comandos o comando abaixo:
	* `pip install virtualenv`;
4. Crie o ambiente virtual para o projeto com os comandos abaixo:
	* `cd Projeto_Acoes`;
	* `virtualenv env_projeto_acoes`;
5. Entre no ambiente virtual através do comando:
	* `env_projeto_acoes\Scripts\activate`;
6. Instale as dependências do projeto através do comando abaixo:
	* `pip install -r requirements.txt`;
7. Para sair do ambiente virtual execute o comando abaixo:
	* `env_projeto_acoes\Scripts\deactivate`.
	
### Como configurar o repositório no Linux - Ubuntu: ###

1. Clonar o repositório:
    * Abra o console de comando do sistema operacional e navegue até o local onde será clonado o repositório;
    * Abra a pagina de [`Overview`](https://bitbucket.org/will_acoes/projeto_acoes/overview) deste repositório, clique em clonar, copie o link HTTPS e execute os comandos abaixo;
    * `git clone link_HTTPS_copiado Projeto_Acoes`;
    * Digite sua senha do Bitbucket quando exigido e `Enter`;
2. Instalar o python3:
	* Execute no prompt de comandos o comando abaixo:
	* `sudo apt update`;
	* `sudo apt-get install python3 python3-pip`;
3. Usando o pip (gerenciador de pacotes do python) instale o virtualenv (ambiente virtual):
	* Execute no prompt de comandos o comando abaixo:
	* `pip install virtualenv`;
4. Crie o ambiente virtual para o projeto com os comandos abaixo:
	* `cd Projeto_Acoes`;
	* `virtualenv env_projeto_acoes`;
5. Entre no ambiente virtual através do comando:
	* `source env_projeto_acoes/bin/activate`;
6. Instale as dependências do projeto através do comando abaixo:
	* `pip install -r requirements.txt`;
7. Para sair do ambiente virtual execute o comando abaixo:
	* `deactivate`.

### Como utilizar esse script no Windows: ###

1. Entre no ambiente virtual através do comando:
	* `env_projeto_acoes\Scripts\activate`;
2. Execute o script através do comando abaixo (ignore os Warnings):
	* `python acoes.py`;
3. Para sair do ambiente virtual execute o comando abaixo:
	* `env_projeto_acoes\Scripts\deactivate`.
	
### Como utilizar esse script no Linux - Ubuntu: ###

1. Entre no ambiente virtual através do comando:
	* `source env_projeto_acoes/bin/activate`;
2. Execute o script através do comando abaixo (ignore os Warnings):
	* `python acoes.py`;
3. Para sair do ambiente virtual execute o comando abaixo:
	* `deactivate`.

### Arquivos gerados pelo script: ###

* O projeto ao executar cria os 6 arquivos abaixo:
	* empresas_advfn.txt - nome das empresas pelo site do advfn;
	* empresas_fundamentus.txt - dados de todas as empresas pelo site fundamentus;
	* empresas_interessantes_fundamentus.txt - dados das empresas com bons valores de análise fundamentalista;
	* arquivo_empresas_gordon.txt - dados das empresas com bons valores de análise fundamentalista e estimativa no modelo de Gordon;
	* empresa_interessantes.xlsx - planilha do excel que efetivamente contém as empresas com bons valores na análise fundamentalista;
	* empresa_interessantes_gordon.xlsx - planilha do excel que contém as empresas com bons valores na análise fundamentalista e uma estimativa da cotação pelo modelo de Gordon.
* Os 4 primeiros desses arquivos não são gerados novamente, se você não apagá-los (servem como cache). Dessa forma, para realizar as operações que geraram estes arquivos novamente, é necessário apagar os arquivos antes de executar o projeto. O algoritmo pode demorar por volta de 23 minutos para rodar.

### Contato: ###

Para perguntas, sugestões ou reportar erros nos contate por Email:

* Willian A.S. (Will1Dexter@hotmail.com)

### Licença: ###

No arquivo `license.txt` e no topo de cada código fonte contém um banner, que não pode ser removido, com informações sobre a licença de uso deste script.

Fique a vontade para nos contatar ou contribuir com este projeto.