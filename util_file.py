#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   util_file.py
#
#   The MIT License (MIT)
#
#   Copyright (c) 2019 Willian <will1dexter@will1dexter-pc>.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy
#   of this software and associated documentation files (the "Software"), to deal
#   in the Software without restriction, including without limitation the rights
#   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#   copies of the Software, and to permit persons to whom the Software is
#   furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#   THE SOFTWARE.
#
#
 
"""
Módulo
 
Este módulo contém um script com funções para manipulação de arquivos.
"""
 
__author__ = "Willian Antônio dos Santos"
__copyright__ = "Copyright 2019"
__credits__ = ["Willian Antônio dos Santos"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Willian Antônio dos Santos"
__email__ = "Will1Dexter@hotmail.com"
__status__ = "Production"

import json
import io
import pickle
import pathlib

# ---- Arquivo texto:
 
def save_text(text, file_name):
    with io.open(file_name, 'w', encoding='utf8') as file_p:
        file_p.write(text)
 
def save_text_iterative(obj_iterable, file_name):
    with io.open(file_name, 'w', encoding='utf8') as file_p:
        for text in obj_iterable:
            text_line = text + '\n'
            file_p.write(text_line)
            file_p.flush()
 
def save_text_list(list_text, file_name):
    save_text_iterative(list_text, file_name)
 
def load_text(file_name):
    data_text = ''
    with io.open(file_name, 'r', encoding='utf8') as file_p:
        data_text = file_p.read()
    return data_text
 
def load_text_iterative(file_name):
    not_read = True
 
    with io.open(file_name, 'r', encoding='utf8') as file_p:
        not_read = False
        for text_line in file_p:
            yield text_line
     
    if(not_read):
        yield ''
 
def load_text_list(file_name):
    data_text = []
    with io.open(file_name, 'r', encoding='utf8') as file_p:
        data_text = file_p.readlines()
    return data_text
 
# ---- Funções para gravação e escrita de objetos através de pickle:
 
def save_file_pkl(obj_to_save, file_name):
    success_save = False
    with open(file_name, 'wb') as output_fp:
        if(hasattr(obj_to_save, 'save_pkl')):
            if(obj_to_save.save_pkl(output_fp)):
                success_save = True
        else:
            pickle.dump(obj_to_save, output_fp, pickle.HIGHEST_PROTOCOL)
            success_save = True
 
    return success_save
 
def load_file_pkl(obj_to_load, file_name):
    success_load = False
 
    obj_has_attr = ((obj_to_load is not None) and hasattr(obj_to_load, 'load_pkl'))
    if(obj_has_attr):
        success_load = None
 
    with open(file_name, 'rb') as input_fp:
        if(obj_has_attr):
            if(obj_to_load.load_pkl(input_fp)):
                success_load = True
        else:
            success_load = pickle.load(input_fp)
 
    return success_load
 
# ---- Json:
 
def save_json(data_dict, file_name):
    data_json = json.dumps(data_dict, ensure_ascii=False, indent=4, sort_keys=False)
    save_text(data_json, file_name)
 
def load_json(file_name):
    data_json = []
    with io.open(file_name, 'r', encoding='utf8') as file_json:
        data_json = json.load(file_json)
    return data_json

# ---- Verificações:

def file_exists(file_name):
    path = pathlib.Path(file_name)
    return (path.exists() and path.is_file())