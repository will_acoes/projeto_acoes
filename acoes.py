#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#   Acoes.py
#
#   The MIT License (MIT)
#
#   Copyright (c) 2019 Willian <will1dexter@will1dexter-pc>.
#
#   Permission is hereby granted, free of charge, to any person obtaining a copy
#   of this software and associated documentation files (the "Software"), to deal
#   in the Software without restriction, including without limitation the rights
#   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#   copies of the Software, and to permit persons to whom the Software is
#   furnished to do so, subject to the following conditions:
#
#   The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
#
#   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#   THE SOFTWARE.
#
#
 
"""
Módulo
 
Este módulo contém um script que avalia ações da bolsa.
"""
 
__author__ = "Willian Antônio dos Santos"
__copyright__ = "Copyright 2019"
__credits__ = ["Willian Antônio dos Santos"]
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Willian Antônio dos Santos"
__email__ = "Will1Dexter@hotmail.com"
__status__ = "Production"

# ---- Imports básicos:
import datetime
from string import ascii_uppercase
import json
from requests.utils import quote
from util_file import save_json, load_json, file_exists
from time_progress import Time_Progress
import warnings
warnings.filterwarnings("ignore")

# ---- Imports de bibliotecas de ciência de dados:
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression

# ---- Imports de bibliotecas de web scraping:
import mechanize
try:
    from http.cookiejar import LWPCookieJar
except ImportError:
    from cookielib import LWPCookieJar
from bs4 import BeautifulSoup
from requests_oauthlib import OAuth1Session

# ---- Parâmetros estáticos:

advfn_link = 'https://br.advfn.com/bolsa-de-valores/bovespa/'
fundamentus_link_base = 'http://www.fundamentus.com.br/'
arquivo_empresas_advfn = 'empresas_advfn.txt'
arquivo_empresas_fundamentus = 'empresas_fundamentus.txt'
arquivo_empresas_interessantes_fundamentus = 'empresas_interessantes_fundamentus.txt'
arquivo_empresas_gordon = 'arquivo_empresas_gordon.txt'
arquivo_empresas_interessantes_excel = 'empresa_interessantes.xlsx'
arquivo_empresas_gordon_excel = 'empresa_interessantes_gordon.xlsx'

# ---- Parâmetros da análise:

div_yield_min_de_corte = 0.06               # Valor de div.yield deve ser estritamente acima deste valor.
ativos_exceto_divida_min_de_corte = 0.0     # Os ativos líquidos menos a dívida líquida deve ser estritamente acima deste valor.
pl_min_de_corte = 0.0                       # Valor de PL deve ser estritamente acima deste valor.
pl_max_de_corte = 18.0                      # Valor de PL deve ser estritamente abaixo deste valor.
periodo_dividendo_min_de_corte = 10         # Deve ter estritamente mais do que essa quantidade de anos de dividendo.

periodo_dividendo_min_de_corte_gordon = 14  # Não elimina a empresa, mas limita os dividendos considerados por um período de anos.
k_gordon = 0.10
g_gordon = 0.01

# ---- Inicialização do Mechanize:
 
def obter_browser():
    brw = mechanize.Browser()
 
    cj = LWPCookieJar()
    brw.set_cookiejar(cj)
 
    brw.set_handle_equiv(True)
    brw.set_handle_gzip(False)
    brw.set_handle_redirect(True)
    brw.set_handle_referer(True)
    brw.set_handle_robots(False)
    brw.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
 
    brw.addheaders = [('User-agent', 'Mozilla/5.0 (X11;\
     U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615\
    Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
     
    return brw

# Browser global.  
br = obter_browser()

# ---- Abertura de conexões:
 
def abrir_link(link, numero_tentativas=3):
    if (numero_tentativas == 0):
        print("[ERRO] Esgotadas as tentativas para abrir o link " + link + ".")
        return ""
        
    try:
        resp = br.open(link)
         
        html = resp.read()
    except Exception:
        html = abrir_link(link, numero_tentativas=numero_tentativas-1)
     
    return html
 
# ---- API do Twitter:

class MyTwitterSearchClient(object):
    MAX_TWEETS = 100
    BASE_URL = "https://api.twitter.com/1.1/search/tweets.json"

    # Essa chave é pessoal e deve ser criada junto ao twitter (um pouco burocrático).
    API_KEY = 'XXXXXXXXXXXXXXXXXXXXXX'
    API_SECRET = 'XXXXXXXXXXXXXXXXXXXXXX'
    ACCESS_TOKEN = 'XXXXXXXXXXXXXXXXXXXXXX'
    ACCESS_TOKEN_SECRET = 'XXXXXXXXXXXXXXXXXXXXXX'
     
     
    def __init__(self):
        self.session = OAuth1Session(self.API_KEY,
                                     self.API_SECRET,
                                     self.ACCESS_TOKEN,
                                     self.ACCESS_TOKEN_SECRET)
     
     
    def get_tweets(self, keyword, n=15, max_id=None):
        if n > 0:
            url = self.BASE_URL + ("?tweet_mode=extended&q=%s&count=%d" % (quote(keyword), n))
            if max_id is not None:
                url = url + "&max_id=%d" % (max_id)
            response = self.session.get(url)
            if response.status_code == 200:
                tweets = json.loads(response.content)
                ids = [tweet['id'] for tweet in tweets['statuses']]
                if(len(ids) > 0):
                    oldest_id = min(ids)-1
                    return tweets['statuses'] + \
                        self.get_tweets(keyword, n-self.MAX_TWEETS, oldest_id)
                else:
                    return tweets['statuses']
        return []

# ---- Funções para obter dados das empresas:

def obter_nomes_empresas_advfn():
    lista_nomes = []
    tp = Time_Progress(length=25, fill='#')
    total_progresso = len(ascii_uppercase)
    print('\nBuscando empresas por pagina no Advfn:')
    tp.print_progress_bar_time(0, total_progresso, msg_before='0 de ' + str(total_progresso))
    for i, letra in enumerate(ascii_uppercase):
        advfn_html_text = abrir_link(advfn_link + letra)
        advfn_soup = BeautifulSoup(advfn_html_text)
        advfn_tabela = advfn_soup.find('table', {'class': 'atoz-link-bov'})
        if(advfn_tabela is not None):
            advfn_tabela = advfn_tabela.find('tbody')
            if(advfn_tabela is not None):
                for linha in advfn_tabela.findAll('tr'):
                    colunas = linha.findAll('td')
                    if ((colunas is not None) and (len(colunas) >= 2)):
                        nome_empresa = colunas[0].find('a').text
                        ticker_empresa = colunas[1].text
                        lista_nomes.append({
                            'nome_empresa': nome_empresa,
                            'ticker_empresa': ticker_empresa
                        })
        tp.print_progress_bar_time((i + 1), total_progresso, msg_before=str(i + 1) + ' de ' + str(total_progresso))
    return lista_nomes

def obter_historico_dividendos(ticker_empresa, cotacao):
    lista_hist_div = []
    div_ano_distinto = None
    div_ano_min = None
    div_ano_max = None
    div_doze_meses = None
    div_yield_real = None
    inclinacao = None

    data_ultimos_doze_meses = datetime.date.today() + datetime.timedelta(-365)
    fundamentus_hist_div = fundamentus_link_base + 'proventos.php?papel=' + ticker_empresa + '&tipo=2'
    hist_div_html_text = abrir_link(fundamentus_hist_div)
    hist_div_soup = BeautifulSoup(hist_div_html_text)
    hist_div_tabela = hist_div_soup.find('table', {'class': 'resultado'})
    if(hist_div_tabela is not None):
        hist_div_tabela = hist_div_tabela.find('tbody')
        if(hist_div_tabela is not None):
            div_doze_meses = 0.0
            lista_hist_div_ano_l = []
            lista_hist_div_pr = []
            lista_hist_div_ano = []
            for linha in hist_div_tabela.findAll('tr'):
                colunas = linha.findAll('td')
                if ((colunas is not None) and (len(colunas) >= 4)):
                    data_hist_div = colunas[0].text.split('/')
                    if(len(data_hist_div) == 3):
                        data_hist_div = datetime.date(int(data_hist_div[2]), int(data_hist_div[1]), int(data_hist_div[0]))
                        lista_hist_div_ano_l.append([data_hist_div.year])
                        if(data_hist_div.year not in lista_hist_div_ano):
                            lista_hist_div_ano.append(data_hist_div.year)
                            if((div_ano_min is None) or (data_hist_div.year < div_ano_min)):
                                div_ano_min = data_hist_div.year
                            if((div_ano_max is None) or (data_hist_div.year > div_ano_max)):
                                div_ano_max = data_hist_div.year
                    else:
                        data_hist_div = None
                    valor_hist_div = float(colunas[1].text.replace(',', '.'))
                    tipo_hist_div = colunas[2].text
                    quantidade_hist_div = int(colunas[3].text)
                    pr = (valor_hist_div / quantidade_hist_div)
                    if(data_hist_div is not None):
                        lista_hist_div_pr.append(pr)
                        if(data_hist_div >= data_ultimos_doze_meses):
                            div_doze_meses += valor_hist_div
                    lista_hist_div.append({
                        'data_hist_div': data_hist_div,
                        'valor_hist_div': valor_hist_div,
                        'tipo_hist_div': tipo_hist_div,
                        'pr': pr,
                        'div_sob_cotacao': (pr / cotacao),
                        'ano': data_hist_div.year,
                        'quantidade_hist_div': quantidade_hist_div
                    })
            model = LinearRegression()
            model.fit(lista_hist_div_ano_l, lista_hist_div_pr)
            div_ano_distinto = len(lista_hist_div_ano)
            inclinacao = model.coef_[0]
            div_yield_real = div_doze_meses / cotacao
    return lista_hist_div, div_ano_distinto, div_ano_min, div_ano_max, div_doze_meses, div_yield_real, inclinacao

def obter_valor_dado(texto, tipo=None):
    if((len(texto) == 0) or (texto.strip() == '-')):    
        return None
    texto_sem_espaco = texto.strip()
    if(tipo is int):
        return int(texto_sem_espaco.replace('.', '').replace('%', ''))
    elif(tipo is float):
        return float(texto_sem_espaco.replace('.', '').replace('%', '').replace(',', '.'))
    else:
        return texto_sem_espaco

def obter_detalhes(ticker_empresa):
    detalhes = None
    fundamentus_detalhe = fundamentus_link_base + 'detalhes.php?papel=' + ticker_empresa + '&tipo=2'
    fundamentus_detalhe_html_text = abrir_link(fundamentus_detalhe)
    fundamentus_detalhe_soup = BeautifulSoup(fundamentus_detalhe_html_text)
    dados_detalhes_tabela = fundamentus_detalhe_soup.findAll('span', {'class': 'txt'})
    if(len(dados_detalhes_tabela) > 95):
        detalhes = {}
        detalhes['cotacao'] = obter_valor_dado(dados_detalhes_tabela[3].text, float)
        detalhes['setor'] = obter_valor_dado(dados_detalhes_tabela[13].text)
        detalhes['valor_mercado'] = obter_valor_dado(dados_detalhes_tabela[21].text, float)
        detalhes['pl'] = obter_valor_dado(dados_detalhes_tabela[32].text, float)
        detalhes['roic'] = obter_valor_dado(dados_detalhes_tabela[64].text, float)
        detalhes['div_yield'] = obter_valor_dado(dados_detalhes_tabela[67].text, float)
        detalhes['divida_liquida'] = obter_valor_dado(dados_detalhes_tabela[93].text, float)
        detalhes['ativos_liquido'] = obter_valor_dado(dados_detalhes_tabela[95].text, float)
    return detalhes


def obter_tweets_textos(nome_empresa, n=15, cliente_twitter = MyTwitterSearchClient()):
    tweets = cliente_twitter.get_tweets(nome_empresa, n)
    save_json(tweets, 'teste.txt')
    tweets = [{
            'text': tw['full_text'],
            'retweeted': tw.get('retweeted_status', {}).get('full_text', '')
        } for tw in tweets]
    return tweets

# ---- Orquestradores da aplicação:

def ler_nomes_empresas():
    empresas = None
    if(file_exists(arquivo_empresas_advfn)):
        empresas = load_json(arquivo_empresas_advfn)
    else:
        empresas = obter_nomes_empresas_advfn()
        save_json(empresas, arquivo_empresas_advfn)
    return empresas

def ler_dados_empresas(empresas):
    empresas_dados = None
    if(file_exists(arquivo_empresas_fundamentus)):
        empresas_dados_serializavel = load_json(arquivo_empresas_fundamentus)
        empresas_dados = []
        for empresa_dado in empresas_dados_serializavel:
            empresa_dado_original = {
                'nome_empresa': empresa_dado['nome_empresa'],
                'ticker_empresa': empresa_dado['ticker_empresa'],
                'lista_hist_div': [],
                'inclinacao': empresa_dado['inclinacao'],
                'div_ano_distinto': empresa_dado['div_ano_distinto'],
                'div_ano_min': empresa_dado['div_ano_min'],
                'div_ano_max': empresa_dado['div_ano_max'],
                'div_doze_meses': empresa_dado['div_doze_meses'],
                'div_yield_real': empresa_dado['div_yield_real'],
                'cotacao': empresa_dado['cotacao'],
                'setor': empresa_dado['setor'],
                'valor_mercado': empresa_dado['valor_mercado'],
                'pl': empresa_dado['pl'],
                'roic': empresa_dado['roic'],
                'div_yield': empresa_dado['div_yield'],
                'divida_liquida': empresa_dado['divida_liquida'],
                'ativos_liquido': empresa_dado['ativos_liquido']
            }
            for hist_div in empresa_dado['lista_hist_div']:
                tuplas_data = hist_div['data_hist_div'].split('-')
                hist_div_original = {
                    'data_hist_div': datetime.date(int(tuplas_data[0]), int(tuplas_data[1]), int(tuplas_data[2])),
                    'valor_hist_div': hist_div['valor_hist_div'],
                    'tipo_hist_div': hist_div['tipo_hist_div'],
                    'pr': hist_div['pr'],
                    'div_sob_cotacao':  hist_div['div_sob_cotacao'],
                    'ano': hist_div['ano'],
                    'quantidade_hist_div': hist_div['quantidade_hist_div']
                }
                empresa_dado_original['lista_hist_div'].append(hist_div_original)
            empresas_dados.append(empresa_dado_original)
    else:
        empresas_dados = []
        empresas_dados_serializavel = []
        tp = Time_Progress(length=25, fill='#')
        total_progresso = len(empresas)
        print('\nBuscando Fundamentus das empresas:')
        tp.print_progress_bar_time(0, total_progresso, msg_before='0 de ' + str(total_progresso))
        for i, empresa in enumerate(empresas):
            detalhes = obter_detalhes(empresa['ticker_empresa'])
            if((detalhes is not None) and (detalhes['cotacao'] != 0.0)):
                lista_hist_div, div_ano_distinto, div_ano_min, div_ano_max, div_doze_meses, div_yield_real, inclinacao = obter_historico_dividendos(empresa['ticker_empresa'], detalhes['cotacao'])
                if(len(lista_hist_div) > 0):
                    empresas_dados.append({
                        'nome_empresa': empresa['nome_empresa'],
                        'ticker_empresa': empresa['ticker_empresa'],
                        'lista_hist_div': lista_hist_div[:],
                        'inclinacao': inclinacao,
                        'div_ano_distinto': div_ano_distinto,
                        'div_ano_min': div_ano_min,
                        'div_ano_max': div_ano_max,
                        'div_doze_meses': div_doze_meses,
                        'div_yield_real': div_yield_real,
                        'cotacao': detalhes['cotacao'],
                        'setor': detalhes['setor'],
                        'valor_mercado': detalhes['valor_mercado'],
                        'pl': detalhes['pl'],
                        'roic': detalhes['roic'],
                        'div_yield': detalhes['div_yield'],
                        'divida_liquida': detalhes['divida_liquida'],
                        'ativos_liquido': detalhes['ativos_liquido']
                    })
                    empresas_dados_serializavel.append({
                        'nome_empresa': empresa['nome_empresa'],
                        'ticker_empresa': empresa['ticker_empresa'],
                        'lista_hist_div': [{
                            'data_hist_div': str(hist_div['data_hist_div']),
                            'valor_hist_div': hist_div['valor_hist_div'],
                            'tipo_hist_div': hist_div['tipo_hist_div'],
                            'pr': hist_div['pr'],
                            'div_sob_cotacao':  hist_div['div_sob_cotacao'],
                            'ano': hist_div['ano'],
                            'quantidade_hist_div': hist_div['quantidade_hist_div']
                        } for hist_div in lista_hist_div],
                        'inclinacao': inclinacao,
                        'div_ano_distinto': div_ano_distinto,
                        'div_ano_min': div_ano_min,
                        'div_ano_max': div_ano_max,
                        'div_doze_meses': div_doze_meses,
                        'div_yield_real': div_yield_real,
                        'cotacao': detalhes['cotacao'],
                        'setor': detalhes['setor'],
                        'valor_mercado': detalhes['valor_mercado'],
                        'pl': detalhes['pl'],
                        'roic': detalhes['roic'],
                        'div_yield': detalhes['div_yield'],
                        'divida_liquida': detalhes['divida_liquida'],
                        'ativos_liquido': detalhes['ativos_liquido']
                    })
            tp.print_progress_bar_time((i + 1), total_progresso, msg_before=str(i + 1) + ' de ' + str(total_progresso))
        save_json(empresas_dados_serializavel, arquivo_empresas_fundamentus)
    return empresas_dados

def empresa_eh_interessante(empresa_dado):
    interessante = (empresa_dado['div_yield_real'] > div_yield_min_de_corte) and \
                   (empresa_dado['ativos_liquido'] - empresa_dado['divida_liquida'] > ativos_exceto_divida_min_de_corte) and \
                   ((empresa_dado['pl'] > pl_min_de_corte) and (empresa_dado['pl'] < pl_max_de_corte)) and \
                   (empresa_dado['div_ano_min'] < (datetime.date.today().year - periodo_dividendo_min_de_corte))
    return interessante

def ler_dados_empresas_interessantes(empresas_dados):
    empresas_interessantes_serializavel = None
    if(file_exists(arquivo_empresas_interessantes_fundamentus)):
        empresas_interessantes_serializavel = load_json(arquivo_empresas_interessantes_fundamentus)
    else:
        empresas_interessantes = [empresa_dado for empresa_dado in empresas_dados if(empresa_eh_interessante(empresa_dado))]

        empresas_interessantes.sort(key=lambda x: x['div_yield_real'], reverse=True)

        empresas_interessantes_serializavel = [{
                        'nome_empresa': empresa_dado['nome_empresa'],
                        'ticker_empresa': empresa_dado['ticker_empresa'],
                        'lista_hist_div': [{
                            'data_hist_div': str(hist_div['data_hist_div']),
                            'valor_hist_div': hist_div['valor_hist_div'],
                            'tipo_hist_div': hist_div['tipo_hist_div'],
                            'pr': hist_div['pr'],
                            'div_sob_cotacao':  hist_div['div_sob_cotacao'],
                            'ano': hist_div['ano'],
                            'quantidade_hist_div': hist_div['quantidade_hist_div']
                        } for hist_div in empresa_dado['lista_hist_div']],
                        'inclinacao': empresa_dado['inclinacao'],
                        'div_ano_distinto': empresa_dado['div_ano_distinto'],
                        'div_ano_min': empresa_dado['div_ano_min'],
                        'div_ano_max': empresa_dado['div_ano_max'],
                        'div_doze_meses': empresa_dado['div_doze_meses'],
                        'div_yield_real': empresa_dado['div_yield_real'],
                        'cotacao': empresa_dado['cotacao'],
                        'setor': empresa_dado['setor'],
                        'valor_mercado': empresa_dado['valor_mercado'],
                        'pl': empresa_dado['pl'],
                        'roic': empresa_dado['roic'],
                        'div_yield': empresa_dado['div_yield'],
                        'divida_liquida': empresa_dado['divida_liquida'],
                        'ativos_liquido': empresa_dado['ativos_liquido']
                    } for empresa_dado in empresas_interessantes]

        save_json(empresas_interessantes_serializavel, arquivo_empresas_interessantes_fundamentus)
    return empresas_interessantes_serializavel

def ler_modelo_gordon(empresas_interessantes_serializavel):
    empresas_gordon_serializavel = None
    if(file_exists(arquivo_empresas_gordon)):
        empresas_gordon_serializavel = load_json(arquivo_empresas_gordon)
    else:
        # Filtar os dividendos a partir dos últimos 14 anos:
        empresas_gordon_serializavel = []
        for empresa_dado in empresas_interessantes_serializavel:
            novo_empresa_dado = {
                                    'nome_empresa': empresa_dado['nome_empresa'],
                                    'ticker_empresa': empresa_dado['ticker_empresa'],
                                    'lista_hist_div': [{
                                        'data_hist_div': hist_div['data_hist_div'],
                                        'valor_hist_div': hist_div['valor_hist_div'],
                                        'tipo_hist_div': hist_div['tipo_hist_div'],
                                        'pr': hist_div['pr'],
                                        'div_sob_cotacao':  hist_div['div_sob_cotacao'],
                                        'ano': hist_div['ano'],
                                        'quantidade_hist_div': hist_div['quantidade_hist_div']
                                    } for hist_div in empresa_dado['lista_hist_div'] if(hist_div['ano'] > (datetime.date.today().year - periodo_dividendo_min_de_corte_gordon))],
                                    'inclinacao': empresa_dado['inclinacao'],
                                    'div_ano_distinto': empresa_dado['div_ano_distinto'],
                                    'div_ano_min': empresa_dado['div_ano_min'],
                                    'div_ano_max': empresa_dado['div_ano_max'],
                                    'div_doze_meses': empresa_dado['div_doze_meses'],
                                    'div_yield_real': empresa_dado['div_yield_real'],
                                    'cotacao': empresa_dado['cotacao'],
                                    'setor': empresa_dado['setor'],
                                    'valor_mercado': empresa_dado['valor_mercado'],
                                    'pl': empresa_dado['pl'],
                                    'roic': empresa_dado['roic'],
                                    'div_yield': empresa_dado['div_yield'],
                                    'divida_liquida': empresa_dado['divida_liquida'],
                                    'ativos_liquido': empresa_dado['ativos_liquido']
                                }
            # Agrupar os dados dos dividendos anualmente:
            dict_hist_div = {}
            for hist_div in novo_empresa_dado['lista_hist_div']:
                dict_hist_div_ano = dict_hist_div.get(hist_div['ano'])
                if(dict_hist_div_ano is None):
                    dict_hist_div_ano = {
                        'lista_hist_div': []
                    }
                dict_hist_div_ano['lista_hist_div'].append(hist_div)
                dict_hist_div[hist_div['ano']] = dict_hist_div_ano

            lista_div_ano = []
            for ano in dict_hist_div:
                dict_hist_div_ano = dict_hist_div.get(ano)
                # Calcular div_ano (dividendo do ano) que é soma dos pr's do ano:
                div_ano = sum([hist_div['pr'] for hist_div in dict_hist_div_ano['lista_hist_div']])
                lista_div_ano.append(div_ano)

            # Ordena os dividendos anuais (crescente):
            lista_div_ano.sort()
            # Estima o dividendo do período (14 anos) através da mediana:
            dividendo = np.median(lista_div_ano)
            novo_empresa_dado['dividendo'] = dividendo
            # Calcula o valor ideal (segundo o modelo de gordon) da cotação das ações da empresa para um valor de 
            # k (constante de retorno de empreendimento ideal) e g (constante de crescimento do dividendo ideal):
            valor_ideal = dividendo / (k_gordon - g_gordon)
            novo_empresa_dado['valor_ideal'] = valor_ideal
            # Calcula o valor estimado de crescimento da cotação a partir do valor_ideal calculado acima em relação a cotação atual:
            crescimento = (valor_ideal - novo_empresa_dado['cotacao']) / novo_empresa_dado['cotacao']
            novo_empresa_dado['crescimento'] = crescimento
            # Conclusão: o crescimento indica o quanto se espera no modelo de gordon que a cotação ainda vá crescer para se chegar no valor ideal pelos seus dividendos.
            empresas_gordon_serializavel.append(novo_empresa_dado)
        save_json(empresas_gordon_serializavel, arquivo_empresas_gordon)
    return empresas_gordon_serializavel

def salvar_empresas_interessantes_excel(empresas_interessantes_serializavel):
    dataframe_empresas = pd.DataFrame([
        [empresa_dado['nome_empresa'], empresa_dado['ticker_empresa'], str(empresa_dado['cotacao']), empresa_dado['setor'], str(empresa_dado['valor_mercado'] / 1000000), str(empresa_dado['pl']),
         str(empresa_dado['roic']), str(empresa_dado['div_yield']), str(empresa_dado['div_yield_real'] * 100), str(empresa_dado['divida_liquida'] / 1000000), str(empresa_dado['ativos_liquido'] / 1000000),
         str(empresa_dado['inclinacao']), str(empresa_dado['div_ano_distinto']), str(empresa_dado['div_ano_min']), str(empresa_dado['div_ano_max']), str(empresa_dado['div_doze_meses'])] 
        for empresa_dado in empresas_interessantes_serializavel],
        columns=['Ação', 'Ticker', 'Cotação', 'Setor', 'Valor de Mercado (mi)', 'Pl', 'Roic', 'Div Yield (%)', 'Div Yield Real (%)', 'Dívida Líquida (mi)', 'Ativos Líquidos (mi)', 'Inclinação', 'Anos Div', 'Anos Div Mínimo', 'Anos Div Máximo', 'Div 12 Mêses'])

    dataframe_empresas.to_excel(arquivo_empresas_interessantes_excel, sheet_name='Ações Interessantes')

def salvar_empresas_gordon_excel(empresas_gordon_serializavel):
    dataframe_empresas = pd.DataFrame([
        [empresa_dado['nome_empresa'], empresa_dado['ticker_empresa'], str(empresa_dado['cotacao']), str(empresa_dado['valor_ideal']), str(empresa_dado['crescimento'] * 100), empresa_dado['setor'], str(empresa_dado['valor_mercado'] / 1000000), str(empresa_dado['pl']),
         str(empresa_dado['roic']), str(empresa_dado['div_yield']), str(empresa_dado['div_yield_real'] * 100), str(empresa_dado['divida_liquida'] / 1000000), str(empresa_dado['ativos_liquido'] / 1000000),
         str(empresa_dado['inclinacao']), str(empresa_dado['div_ano_distinto']), str(empresa_dado['div_ano_min']), str(empresa_dado['div_ano_max']), str(empresa_dado['div_doze_meses']), str(empresa_dado['dividendo'])] 
        for empresa_dado in empresas_gordon_serializavel],
        columns=['Ação', 'Ticker', 'Cotação', 'Valor Ideal Cotação Gordon', 'Crescimento Cotação Gordon (%)', 'Setor', 'Valor de Mercado (mi)', 'Pl', 'Roic', 'Div Yield (%)', 'Div Yield Real (%)', 'Dívida Líquida (mi)', 'Ativos Líquidos (mi)', 'Inclinação', 'Anos Div', 'Anos Div Mínimo', 'Anos Div Máximo', 'Div 12 Mêses', 'Mediana Dividendo Anual'])

    dataframe_empresas.to_excel(arquivo_empresas_gordon_excel, sheet_name='Ações Interessantes Modelo Gordon')

def buscar_twitter_empresa(empresas_interessante):
    cliente_twitter = MyTwitterSearchClient()
    save_json(obter_tweets_textos(empresas_interessante['nome_empresa'], 120, cliente_twitter), 'empresa_twitter.txt')
    save_json(obter_tweets_textos(empresas_interessante['ticker_empresa'], 120, cliente_twitter), 'empresa_ticker_twitter.txt')
    nome_empresa_com_hash = '#' + ''.join([trecho.capitalize() for trecho in empresas_interessante['nome_empresa'].lower().replace('on', '').replace('pna', '').replace('pnb', '').split()])
    save_json(obter_tweets_textos(nome_empresa_com_hash, 120, cliente_twitter), 'empresa_twitter_hash.txt')
    save_json(obter_tweets_textos('#' + empresas_interessante['ticker_empresa'], 120, cliente_twitter), 'empresa_ticker_twitter_hash.txt')

# ---- Aplicação:

def main():
    empresas = ler_nomes_empresas()
    empresas_dados = ler_dados_empresas(empresas)
    empresas_interessantes_serializavel = ler_dados_empresas_interessantes(empresas_dados)
    empresas_gordon_serializavel = ler_modelo_gordon(empresas_interessantes_serializavel)
    salvar_empresas_interessantes_excel(empresas_interessantes_serializavel)
    salvar_empresas_gordon_excel(empresas_gordon_serializavel)

    #buscar_twitter_empresa(empresas_interessantes_serializavel[0])

if __name__ == "__main__":
    main()

